#ifndef SELF_B_ROBOT
#define SELF_B_ROBOT

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <compat/twi.h>
#include <util/delay.h>
#include <string.h>
#include <math.h>

#include "../drivers/pwm.h"
#include "../drivers/encoder.h"
#include "../external/uart.h"
#include "../features/regulation.h"
#include "../drivers/motors.h"
#include "../external/mpu6050.h"

//macros utilities
#define _set_(reg, bit) (reg |= 1 << bit)
#define _clr_(reg, bit) (reg &= ~(1 << bit))
#define _toggle_(reg, bit) (reg ^= 1 << bit)

#endif

