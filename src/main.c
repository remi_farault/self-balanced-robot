#include "common/util.h"

int main()
{	
	uart_init(9600);
	Mpu6050_AutoInit();
	init_fast_pwm();
	init_motors();
	init_encoders();
	init_regulation();
	sei();

    while(true)
    {
		asm volatile ("sleep");
	}
    return 0 ;
}
