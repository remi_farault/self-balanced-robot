#include "../common/util.h"

void motors_update(uint16_t command_a, uint16_t command_b, direction_t d_a, direction_t d_b)
{
    if(d_a == REVERSE)
    {
        PORTB |= _BV(PB0);
        PORTD |= _BV(PD7);
        PORTD &= ~(_BV(PD6));
    }
    else if(d_a == FORWARD)
    {
        PORTB |= _BV(PB0);
        PORTD |= _BV(PD6);
        PORTD &= ~(_BV(PD7));
    }
    else
    {
        PORTD&= ~(_BV(PD6) | _BV(PD7));
        return;
    }

    if(d_b == REVERSE)
    {
        PORTB |= _BV(PB0) | _BV(PB5);
        PORTB &= ~(_BV(PB4));
    }
    else if(d_b == FORWARD)
    {
        PORTB |= _BV(PB0) | _BV(PB4);
        PORTB &= ~(_BV(PB5));
    }
    else
    {
        PORTB &= ~(_BV(PB4) | _BV(PB5));
        return;
    }
    generate_fast_pwm(command_a, command_b);

}

void init_motors()
{
    //motors pins
    DDRD |= _BV(PD7) | _BV(PD6);
    DDRB |= _BV(PB0) | _BV(PB4) | _BV(PB5) | _BV(PB1) | _BV(PB2);
}

    
    