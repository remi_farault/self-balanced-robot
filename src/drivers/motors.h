#ifndef MOTORS
#define MOTORS

typedef enum  { FORWARD, REVERSE, STOP } direction_t;

void motors_update(uint16_t command_a, uint16_t command_b, direction_t d_a, direction_t d_b);
void init_motors();

#endif