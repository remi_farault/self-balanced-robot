#include "../common/util.h"

void init_fast_pwm()
{
    //pwm init
    TCCR1A |= _BV(COM1A1) | _BV(COM1B1) | _BV(WGM11);
    TCCR1B |= _BV(WGM12) | _BV(WGM13) | _BV(CS10);
    ICR1 = PWM_ICR1;
    OCR1A = 0;
    OCR1B = 0;
}

void generate_fast_pwm(uint16_t pwm_a, uint16_t pwm_b)
{
    OCR1A = pwm_a >= PWM_ICR1 ? PWM_ICR1 : pwm_a;
    OCR1B = pwm_b >= PWM_ICR1 ? PWM_ICR1 : pwm_b;
}
