
#include "../common/util.h"
#include "../external/uart.h"

uint8_t count_a, count_b;

ISR(PCINT2_vect)
{
  static uint8_t previous_state = 0;

  if((PIND & _BV(PD2)) && !(previous_state & (_BV(PD2))))
  {
    //rising edge detected from PD2
    count_a++;
  }
  else if((PIND & _BV(PD4)) && !(previous_state & (_BV(PD4))))
  {
    //rising edge detected from PD4
    count_b++;
  }
  
  previous_state = PIND;
}

void init_encoders()
{
  //set PD2 PD4 as inputs
  DDRD &= ~(_BV(PD2) | _BV(PD4));

  //Pin Change interrupt configuration
  PCICR |= _BV(PCIE2);
  PCMSK2 |= _BV(PCINT18) | _BV(PCINT20);
}

