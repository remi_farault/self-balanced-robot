
#ifndef PWM
#define PWM

#define PWM_ICR1 1000

void init_fast_pwm();

void generate_fast_pwm(uint16_t left_pwm, uint16_t right_pwm);

#endif