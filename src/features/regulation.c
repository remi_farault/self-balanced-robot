#include "../common/util.h"

extern uint8_t count_a, count_b;
float angle_x;
int consigne_angle = 0;
int consigne_speed = 0;//linear speed
int consigne_orientation = 0;//orientation
int speed_motor_a;
int speed_motor_b;

pid_regulator_t reg_motor_a = 
{
  .kp = 1,
  .ki = 0,
  .kd  = 0,
  .last_error = 0,
  .sum_error = 0
};

pid_regulator_t reg_motor_b = 
{
  .kp = 1,
  .ki = 0,
  .kd  = 0,
  .last_error = 0,
  .sum_error = 0
};

pid_regulator_t reg_angle = 
{
  .kp = 30,
  .ki = 0.1,
  .kd = 0.58,
  .last_error = 0,
  .sum_error = 0
};


pid_regulator_t reg_speed = 
{
  .kp = 5,
  .ki = 0,
  .kd = 0,
  .last_error = 0,
  .sum_error = 0
};


ISR(TIMER0_OVF_vect)
{
    //measure motors speeds and directions
    speed_motor_a = PORTD & _BV(PD6) ? count_a*ENCODER_COEFF : -count_a*ENCODER_COEFF;
    speed_motor_b = PORTB & _BV(PB4) ? count_b*ENCODER_COEFF : -count_b*ENCODER_COEFF;
    count_a = 0;
    count_b = 0;

    //linear speed regulation
    int lin_speed = (speed_motor_a + speed_motor_b)/2;
    int target_angle = pid(&reg_speed,consigne_speed-lin_speed);

    //angle regulation
    Mpu6050_GetAngleX(&angle_x);
    angle_x = (angle_x > 180 ? 11.5 + angle_x - 360 : 11.5 + angle_x);  //to get angle between -180 and 180 degrees
    int target_motors = pid(&reg_angle,target_angle+consigne_angle-angle_x);
    
    //motors regulation
    int command_motor_a = pid(&reg_motor_a,consigne_orientation+target_motors-speed_motor_a);
    int command_motor_b = pid(&reg_motor_b,target_motors-speed_motor_b-consigne_orientation);
    
    direction_t d_a, d_b;
    if(command_motor_a > 0) d_a = FORWARD;
    else if(command_motor_a < 0) d_a = REVERSE;
    else if(command_motor_a == 0) d_a = STOP;
    if(command_motor_b > 0) d_b = FORWARD;
    else if(command_motor_b < 0) d_b = REVERSE;
    else if(command_motor_b == 0) d_b = STOP;

    motors_update(abs(command_motor_a),abs(command_motor_b),d_a,d_b);
    printf("%.2f\n",angle_x); 
}

void init_regulation()
{
  //init timer 2 to refresh robot commands periodically
  TCCR0A = 0;
	TCCR0B |= _BV(CS02);
	TIMSK0 |= _BV(TOIE0);
}

//compute regulation commands 
int pid(pid_regulator_t *reg, int error)
{   
    if(reg->sum_error + error*DT >= MAX_SUM_ERROR){ reg->sum_error = MAX_SUM_ERROR; }
    else if(reg->sum_error + error*DT <= -MAX_SUM_ERROR) {reg->sum_error = -MAX_SUM_ERROR;}
    else { reg->sum_error += error*DT; }

    int command = (int)(reg->kp * error + reg->ki*reg->sum_error + reg->kd * (error- reg->last_error)/DT);
    reg->last_error = error;

    return command;
}