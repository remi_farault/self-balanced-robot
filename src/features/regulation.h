#ifndef REGULATION
#define REGULATION

#define MAX_SUM_ERROR 32000
#define REG_FREQ 244
#define DT 1/REG_FREQ

typedef struct
{
  int last_error;
  int16_t sum_error;
  double kp,ki,kd;
}
pid_regulator_t;


void init_regulation();
int pid(pid_regulator_t *reg, int error);

#endif