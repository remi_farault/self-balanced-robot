.PHONY: clean

CC = avr-gcc
OBJ_COPY = avr-objcopy
FLASH = avrdude
FORMAT = hex
MCU = atmega328p
MCU_ID = m328p
F_CPU = 16000000
COM = /dev/ttyACM0
BAUD_RATE = 115200
CFLAGS = -Wall -Wextra -g  -O3 -DF_CPU=$(F_CPU) -mmcu=$(MCU) -Wl,-u,vfprintf -lprintf_flt -lm
SOURCE_DIR = src
BUILD_DIR = build
OBJ_DIR = $(BUILD_DIR)/obj
DIRS = drivers common features external

SOURCES := $(shell find $(SOURCE_DIR) -name '*.c')
OBJECTS = $(SOURCES:.c=.o)
EXEC = self-balanced-robot

ifeq "$(FORMAT)" "hex"
OPTION = ihex
else
OPTION = binary
endif

all: dirs bin

dirs:
	mkdir -p $(foreach DIR,$(DIRS),$(OBJ_DIR)/$(DIR))
	
bin: elf
	$(OBJ_COPY)  -O $(OPTION) $(BUILD_DIR)/$(addsuffix .elf, $(EXEC)) $(BUILD_DIR)/$(addsuffix .$(FORMAT), $(EXEC))

elf: $(OBJECTS)
	$(CC) $(CFLAGS) $(subst $(SOURCE_DIR),$(OBJ_DIR),$^) -o $(BUILD_DIR)/$(addsuffix .elf, $(EXEC))
	
%.o: %.c 
	$(CC) $(CFLAGS) -c $^ -o $(subst $(SOURCE_DIR),$(OBJ_DIR),$@)


flash: bin
	$(FLASH) -v -c arduino -p $(MCU_ID) -P $(COM) -b $(BAUD_RATE) -D -U flash:w:$(BUILD_DIR)/$(addsuffix .$(FORMAT), $(EXEC)):i
	
clean:
	rm -rf $(subst $(SOURCE_DIR),$(OBJ_DIR),$(OBJECTS)) $(BUILD_DIR)/$(addsuffix .hex, $(EXEC))  $(BUILD_DIR)/$(addsuffix .bin, $(EXEC)) $(BUILD_DIR)/$(addsuffix .elf, $(EXEC))
