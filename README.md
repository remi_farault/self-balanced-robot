# Self Balancing Robot 

## Description

An AVR C program used to control a self-balancing robot 

The robot's hardware comes from an Adeept Self Balancing Robot Kit and includes the components listed below

## Components

+ Arduino UNO -> Atmega328p
+ Adeept Self Balancing Robot Shield
+ TB6612FNG Motor Driver
+ GY-521 Gyroscope/Accelerometer -> MPU6050
+ HC-SR04 Ultrasonic Sensor
+ 6V DC Motors with optical encoders

